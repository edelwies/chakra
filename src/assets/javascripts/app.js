// Main Application Module
// -----------------------
// This module is the start point of application.
var App = angular.module("Application", ["ngTouch", "ngAnimate",  "gettext","ionic",
                                         "angular-gestures", "Chakra", "ORM"]);

// configuration section ---------------------------

App.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider){
     $urlRouterProvider.otherwise("/");
    // Configuring application index route.
    // Add any route you need here.
    $stateProvider.
        state("root", {
            url: "/",
            templateUrl: template_url("main"),
            controller: "MainController"
        });
}]);

App.controller("MainController", ["$scope", "gettext", "orm", "notify", function($scope, gettext, orm, $notify){
    // Main controller of application. This controller is responsible for `/` url
    orm.init();
    $scope.exitFromApp = function() {
        $notify.exit();
    };
}]);
