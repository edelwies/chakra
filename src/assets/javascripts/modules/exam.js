// Chakra Module
// -----------------------
// This module is responsible for application settings
var Chakra = angular.module("Chakra", ["gettext", "ORM", "ionic", "Notify"]);

// configuration section ---------------------------
Chakra.config(["$stateProvider", function($stateProvider){

    // Configuring application index route.
    // Add any route you need here.
    $stateProvider.
        state("chakra", {
            url: "/start/:id",
            templateUrl: template_url("chakra/index"),
            controller: "ChakrasController"
        }).
        state("result", {
            url: "/result/",
            templateUrl: template_url("chakras/result"),
            controller: "ResultChakrasController"
        }).
        state("description", {
            url: "/description",
            templateUrl: template_url("chakras/description"),
            controller: "DescController"
        }).
        state("description_chakra", {
             url: "/chakra-list",
             templateUrl: template_url("chakras/description_chakra"),
              controller: "ChakradescController"
        });


}]);

Chakra.controller("ChakrasController", ["$scope", "gettext", "orm", "notify", "$location", "$stateParams", function($scope, gettext, orm, $notify, $location, $stateParams){
    // Chakra controller of application. This controller is responsible for `/game` url


    // -----------------------------------

    $scope.get_question = function(id) {
        $scope.question = orm.find(id);

        if($scope.question === undefined ) {
            $scope.process_answers();
        }
    };


    $scope.data = {
        clientSide: 'ng'
    };

    $scope.set_user_answer = function(answer){
        $scope.user_answer = answer;
        var next_id = parseInt(id) + 1;

    };

    $scope.correct = function(id) {
        orm.set_answer(id,
                       "بله",
                       0);
        var next_id = parseInt(id) + 1;
        $location.path("/start/" + next_id);

    };

    $scope.not_correct = function(id) {
        orm.set_answer(id,
                       "خیر",
                       1);
        var next_id = parseInt(id) + 1;
        $location.path("/start/" + next_id);

    };

    $scope.next = function(id) {
        if ( $scope.user_answer === undefined ){
            $notify.alert("یکی از گزینه‌ها را انتخاب کنید", undefined ,"خطا","تایید");
        }else{
            orm.set_answer(id, $scope.user_answer, _.indexOf($scope.question.answers, $scope.user_answer));
            var next_id = parseInt(id) + 1;
            $location.path("/start/" + next_id);
        }
    };

    $scope.process_answers = function(){
        var answers = orm.answers();
        $location.path("/result/");
    };

    $scope.get_question($stateParams.id);

}]);


Chakra.controller("ResultChakrasController", ["$scope", "gettext", "orm", "notify", "$location", "$stateParams", function($scope, gettext, orm, $notify, $location, $stateParams){
    // Chakra controller of application. This controller is responsible for `/game` url

    var count = 0;
    _.each(orm.answers(), function(x) {
        var question = orm.find(x[0]);
        if (question.correct_answers == x[2]){
            count = count + 1;
        }

    });

     $(".fcontainer").height(window.screen.height / 2.5);
}]);

Chakra.controller("DescController", ["$scope", "gettext", "orm", "notify", "$location", "$stateParams", "$ionicScrollDelegate", function($scope, gettext, orm, $notify, $location, $stateParams,  $ionicScrollDelegate){

}]);

Chakra.controller("ChakradescController", ["$scope", "gettext", "orm", "notify", "$location", "$stateParams", "$ionicScrolDelegate", function($scope, gettext,orm, $notify, $location, $stateParams, $ionicScrollDelegate) {

}]);
