// A proxy notification service
var Notify = angular.module('Notify', []);

Notify.service('notify', [function(){
    var that = this;
    this.alert = function(msg, callback, title, buttons) {
        if (navigator.notification !== undefined) {
            navigator.notification.alert(msg, callback, title, buttons);
        }
        else {
            alert(msg);
        }
    };

}]);
